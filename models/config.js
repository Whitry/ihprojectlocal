var mongoose = require('./../libs/mongoose'),
  Schema = mongoose.Schema;

var SchemaTypes = mongoose.Schema.Types;

var schema = new Schema({
  key: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: false
  }
});

exports.iConfig = mongoose.model('iConfig', schema);