/*
* // link to iMap
 map: {
 type: Schema.ObjectId,
 required: false,
 default: null
 },
 // position on map
 px: {
 type: Number
 },
 py: {
 type: Number
 },
 //
* */

// Изображение, на которое нанесены датчики

var mongoose = require('./../libs/mongoose'),
  Schema = mongoose.Schema;



var schema = new Schema({
  map: {
    type: Schema.ObjectId,
    required: true
  },
  sensor: {
    type: Schema.ObjectId,
    required: true
  },
  // position on map
  px: {
    type: Number,
    required: false
  },
  py: {
    type: Number,
    required: false
  },
  /*
  * Номер  ПО ПОРЯДКУ
  * */
  position: {
    type: Number,
    required: true
  }

});

exports.MapSensorLink = mongoose.model('MapSensorLink', schema);