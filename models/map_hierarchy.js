// Иерархия между картами

var mongoose = require('./../libs/mongoose'),
    Schema = mongoose.Schema;
require('./../libs/mongoose-long')(mongoose);

var schema = new Schema({
    map_from: {
        type: Schema.ObjectId,
        required: true
    },
    map_to: {
        type: Schema.ObjectId,
        required: true
    },

    // в формате json информация об объекте на рисунке
    info_json: {
        type: String,
        required: true
    }
});

exports.MapHierarchy = mongoose.model('MapHierarchy', schema);