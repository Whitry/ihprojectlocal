// Изображение, на которое нанесены датчики

var mongoose = require('./../libs/mongoose'),
    Schema = mongoose.Schema;

var SchemaTypes = mongoose.Schema.Types;

var schema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
  have_plan:{
    type: Boolean,
    required: false,
    default: false
  },
  map_order: {
    type: Number,
    required: false,
    unique: true,
    default: 0
  },
    image_url: {
        type: String,
        required: true
    }
});

exports.iMap = mongoose.model('iMap', schema);