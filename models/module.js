var async = require('async');
var util = require('util');

var mongoose = require('./../libs/mongoose'),
  Schema = mongoose.Schema;


var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  active: {
    type: Boolean,
    required: true,
    default: false
  }
});

exports.iModule = mongoose.model('iModule', schema);