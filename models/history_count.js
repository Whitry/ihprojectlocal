// Сохраненные данные датчиков, которые можно посчитать
var mongoose = require('libs/mongoose'),
  Schema = mongoose.Schema;
require('libs/mongoose-long')(mongoose);

var SchemaTypes = mongoose.Schema.Types;

var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  sensor:{
    type: Schema.ObjectId,
    required: true
  },
  value:{
    type: SchemaTypes.Long,
    required: true
  },
  last_update: {
    type: Date,
    default: Date.now
  }
});

exports.HCount = mongoose.model('HCount', schema);