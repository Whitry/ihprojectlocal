// Сохраненные данные датчиков, которые нельзя посчитать
var mongoose = require('libs/mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  sensor:{
    type: Schema.ObjectId,
    required: true
  },
  value:{
    type: String,
    required: true
  },
  last_update: {
    type: Date,
    default: Date.now
  }
});

exports.HUnCount = mongoose.model('HUnCount', schema);