var async = require('async');
var util = require('util');
var Enum = require('enum');

var mongoose = require('./../libs/mongoose'),
    Schema = mongoose.Schema;


var schema = new Schema({
    external_id: {
        type: String,
        required: false,
        unique: true
    },
    // link to sensor_type
  /*
  * 1 температура
  * 2 включение выключение света
  * 3 диммер
  * 4 дверь открыто/закрыто
  * 5 освещенность
  * 6 протечка
  * 8
  * */
    type: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    // current value
    value: {
        type: String,
        required: false
    },
    priority: {
        type: Number,
        required: false
    },
    // Система, которой принадлежит датчик
    // z-wave, noolite и т.д.
    system: {
        type: String,
        required: true
    },
    // в формате json дополнительная информация
    info_json: {
      type: String,
      required: false
    },
    last_update: {
        type: Date,
        default: Date.now
    }
});

schema.methods.getTemplate = function(){
  switch (this.type) {
    case 1:
      return "temp";
    case 2:
      return "switcher";
    case 3:
      return "dimmer";
    case 4:
      return "door";
    case 5:
      return "smultilevel";
    case 6:
      return "";
  }
  return "common";
}

var SensorTypeList = new Enum({'Temp': 1, 'DoorWindow' : 4});
global.sensor_types = SensorTypeList;

exports.Sensor = mongoose.model('Sensor', schema);