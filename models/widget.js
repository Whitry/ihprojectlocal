var async = require('async');

var mongoose = require('./../libs/mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema({
  target: {
    type: String,
    required: true
  },
  template: {
    type: String

  },
  // в формате json дополнительная информация
  info_json: {
    type: String,
    required: false
  },
  row: {
    type: String,
    required: false
  },
  column: {
    type: String,
    required: false
  },
  height: {
    type: String,
    required: false
  },
  width: {
    type: String,
    required: false
  }
});


exports.Widget = mongoose.model('Widget', schema);