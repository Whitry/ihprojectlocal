var mongoose = require('./../libs/mongoose'),
    Schema = mongoose.Schema;
require('./../libs/mongoose-long')(mongoose);

var SchemaTypes = mongoose.Schema.Types;

var schema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    sensor:{
        type: Schema.ObjectId,
        required: true
    },
    // Уровень тревоги
    // 1 - самый нижний
    // 4 - наивысший
    level:{
        type:Number,
        default:1
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});

exports.iEvent = mongoose.model('iEvent', schema);