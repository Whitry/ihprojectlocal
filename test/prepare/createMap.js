var async = require('async');


exports.createData = function (mongoose, external_callback) {


  async.series([
    requireModels,
    createMaps,
    showMaps
  ], function (err) {
    if (err) console.log('ERROR: ', err);
    console.log(arguments);
    external_callback();
  });


  function requireModels(callback) {
    require('./../../models/map').iMap;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
  }

  function createMaps(callback) {

    var maps = [
      { title: "Общий план", image_url: "images/map/main.png", map_order: 0},
      { title: "Этаж 1", image_url: "images/map/main.png", map_order: 1},
      { title: "Этаж 3", image_url: "images/map/main.png", map_order: 2},
      { title: "Этаж 4", image_url: "images/map/main.png", map_order: 3},
      { title: "Этаж 5", image_url: "images/map/main.png", map_order: 4},
      { title: "Этаж 6", image_url: "images/map/main.png", map_order: 5},
      { title: "Этаж 7", image_url: "images/map/main.png", map_order: 6},
      { title: "Этаж 8", image_url: "images/map/main.png", map_order: 7}
    ];
    async.each(maps, function (userData, callback) {
      var map = new mongoose.models.iMap(userData);
      console.log(map);
      map.save(callback);
    }, callback);


  };

  function showMaps(callback) {

    mongoose.models.iMap.find({}, function (err, result) {
      console.log(result);
    });
    callback();
  }
}