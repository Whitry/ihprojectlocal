var async = require('async');


exports.createData = function (mongoose, external_callback) {


  async.series([
    requireModels,
    createUser,
    showUser
  ], function (err) {
    if (err) console.log('ERROR: ', err);
    console.log(arguments);
    external_callback();
  });

  function requireModels(callback) {
    require('./../../models/user').Sensor;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
  }

  function createUser(callback) {

    var data = [
      { username: "User1", email: "some@some.ru", password: "123"},
      { username: "User2", email: "some1@some.ru", password: "123"},
      { username: "User3", email: "some2@some.ru", password: "123", group:'admin'}
    ];
    async.each(data, function (userData, callback) {
      var user = new mongoose.models.User(userData);
      console.log(user);
      user.save(callback);
    }, callback);


  };

  function showUser(callback) {

    mongoose.models.Sensor.find({}, function (err, result) {
      console.log(result);
    });
    callback();
  }
}