var async = require('async');


exports.createData = function (mongoose, external_callback) {


  async.series([
    requireModels,
    createConfig
  ], function (err) {
    if (err) console.log('ERROR: ', err);
    console.log(arguments);
    external_callback();
  });

  function requireModels(callback) {
    require('./../../models/config').iConfig;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
  }

  function createConfig(callback) {

    var data = [
      { key: "module.alarm", value: ""}
    ];
    async.each(data, function (userData, callback) {
      var user = new mongoose.models.iConfig(userData);
      console.log(user);
      user.save(callback);
    }, callback);


  };

}