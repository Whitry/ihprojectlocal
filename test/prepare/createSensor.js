var async = require('async');


exports.createData = function (mongoose, external_callback) {


  async.series([
    requireModels,
    createSensors,
    showSensors
  ], function (err) {
    if (err) console.log('ERROR: ', err);
    console.log(arguments);
    external_callback();
  });

  function requireModels(callback) {
    require('./../../models/sensor').Sensor;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
  }

  // 1  - температура

  function createSensors(callback) {

    var maps = [
      { external_id: 'devices.5.instances.0.data', type: 4, name: "Входная дверь", value: "0", priority: 1, system:"z-wave"},
      { external_id: 'devices.2.instances.2.data', type: 1, name: "Комната", value: "14.6", priority: 1, system:"z-wave"},
      { external_id: 'devices.3.instances.0.data', type: 1, name: "Дверь", value: "22.1", priority: 1, system:"z-wave"},
      { external_id: 'devices.8.instances.0.data', type: 5, name: "Жалюзи", value: "0", priority: 1, system:"z-wave"},
      { external_id: 'devices.6.instances.0.data', type: 3, name: "Диммер", value: "0", priority: 1, system:"z-wave"},
      { external_id: 'devices.9.instances.0.data', type: 1, name: "Офис", value: "0", priority: 1, system:"z-wave"},

      { external_id: 'devices.12.instances.1.data', type: 3, name: "Синий", value: "0", priority: 1, system:"z-wave"},
      { external_id: 'devices.12.instances.2.data', type: 3, name: "Красный", value: "0", priority: 1, system:"z-wave"},
      { external_id: 'devices.12.instances.3.data', type: 3, name: "Зеленый", value: "0", priority: 1, system:"z-wave"},


//      { external_id: 'devices.54.instances.0.data', type: 4, name: "Входная дверь4", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.55.instances.0.data', type: 4, name: "Входная дверь5", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.56.instances.0.data', type: 4, name: "Входная дверь6", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.57.instances.0.data', type: 4, name: "Входная дверь7", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.58.instances.0.data', type: 4, name: "Входная дверь8", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.59.instances.0.data', type: 4, name: "Входная дверь9", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.512.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.513.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.514.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.515.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.516.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.517.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.518.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},
//      { external_id: 'devices.519.instances.0.data', type: 4, name: "Входная дверь11", value: "0", priority: 1, system:"z-wave"},


    ];
    async.each(maps, function (userData, callback) {
      var map = new mongoose.models.Sensor(userData);
      console.log(map);
      map.save(callback);
    }, callback);


  };

  function showSensors(callback) {

    mongoose.models.Sensor.find({}, function (err, result) {
      console.log(result);
    });
    callback();
  }
}