var mongoose = require('./../../libs/mongoose');
var async = require('async');


exports.createData = function (mongoose, external_callback) {
  var maps;
  var sensors;

  async.series([
    requireModels,
    findMaps,
    findSensors,
    createMapSensorLink,
    showMapSensorLink
  ], function (err) {
    if (err) console.log('ERROR: ', err);
    //console.log(arguments);
    external_callback();
  });


  function requireModels(callback) {
    require('./../../models/map_sensor_link').MapSensorLink;

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
      mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
    require('./../../models/map').iMap;
    require('./../../models/sensor').Sensor;
  }

  function findMaps(callback){
    mongoose.models.iMap.find({}, function (err, result) {
      maps = result;
      callback();
    });
  }

  function findSensors(callback){
    mongoose.models.Sensor.find({}, function (err, result) {
      sensors = result;
      callback();
    });
  }

  function createMapSensorLink(callback) {

    var link_maps = [
      { map: maps[0], sensor: sensors[0], position: 1},
      { map: maps[0], sensor: sensors[1], position: 2},
      { map: maps[0], sensor: sensors[2], position: 3},
      { map: maps[0], sensor: sensors[3], position: 4},
      { map: maps[0], sensor: sensors[4], position: 5},

      { map: maps[0], sensor: sensors[5], position: 6},
      { map: maps[0], sensor: sensors[6], position: 5},
      { map: maps[0], sensor: sensors[7], position: 5},
      { map: maps[0], sensor: sensors[8], position: 5},
//      { map: maps[0], sensor: sensors[11], position: 5},
//      { map: maps[0], sensor: sensors[12], position: 5},
//      { map: maps[0], sensor: sensors[13], position: 5},
//      { map: maps[0], sensor: sensors[14], position: 5},
//      { map: maps[0], sensor: sensors[15], position: 5},
//      { map: maps[0], sensor: sensors[16], position: 5},
//      { map: maps[0], sensor: sensors[17], position: 5},
//      { map: maps[0], sensor: sensors[18], position: 5},
//      { map: maps[0], sensor: sensors[19], position: 5},
//      { map: maps[0], sensor: sensors[20], position: 5},
//      { map: maps[0], sensor: sensors[21], position: 5}

    ];
    async.each(link_maps, function (userData, callback) {
      var map = new mongoose.models.MapSensorLink(userData);
      console.log(map);
      map.save(callback);
    }, callback);


  };

  function showMapSensorLink(callback) {

    mongoose.models.MapSensorLink.find({}, function (err, result) {
      console.log(result);
    });
    callback();
  }
}