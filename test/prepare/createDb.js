var async = require('async');
var mongoose = require('./../../libs/mongoose');
var mapCreator = require('./createMap');
var sensorCreator = require('./createSensor');
var linkMapSensor = require('./createLinkMapSensor');
var userCreator = require('./createUser');
var configCreator = require('./createConfig');

async.series([
  open,
  dropDatabase,
  createSensors,
  createMaps,
  createLinks,
  createConfig,
  createUsers

], function (err) {
  if (err) console.log('ERROR: ', err);
  console.log(arguments);
  mongoose.disconnect();
  process.exit(err ? 255 : 0);
});

function open(callback) {
  mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
  var db = mongoose.connection.db;
  db.dropDatabase(callback);
}

function createMaps(callback) {
  mapCreator.createData(mongoose, callback);
}

function createSensors(callback) {
  sensorCreator.createData(mongoose, callback);
}

function createLinks(callback) {
  linkMapSensor.createData(mongoose, callback);
}

function createUsers(callback) {
  userCreator.createData(mongoose, callback);
}

function createConfig(callback) {
  configCreator.createData(mongoose, callback);
}