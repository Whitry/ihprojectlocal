var zmq = require('zmq')
  , clientPort = 'tcp://127.0.0.1:12345'
  , serverPort = 'tcp://127.0.0.1:12346'
  , queueNameClient = 'zwave_client'
  , queueNameServer = 'zwave_server';


var socketClient = zmq.socket('pub');

socketClient.identity = 'client' + process.pid;
socketClient.connect(clientPort);

function createClient2 (port, value) {
  socketClient.send(value);
};

//global.zwaveEvent.on('doorOpen', function(){
//  console.log('event emitter!');
//  //createClient2(clientPort, queueNameClient + ' adads');
//});


function createWorker (port) {
  var socket = zmq.socket('sub');

  socket.identity = 'worker' + process.pid;

  //socket.subscribe(queueNameServer);
  socket.subscribe('');
  socket.on('message', function(data) {
    console.log(socket.identity + ': got ' + data.toString());
    global.zeromqEvent.emit('message', data.toString());
  });

  socket.connect(port, function(err) {
    if (err) throw err;
    console.log('worker connected!');
  });
};

function createForwarderDevice(frontPort, backPort) {
  var frontSocket = zmq.socket('sub'),
    backSocket = zmq.socket('pub');

  frontSocket.identity = 'sub' + process.pid;
  backSocket.identity = 'pub' + process.pid;

  frontSocket.subscribe(queueNameServer);
  frontSocket.bind(frontPort, function (err) {
    console.log('bound', frontPort);
  });

  frontSocket.on('message', function() {
    //pass to back
    console.log('forwarder: recasting', arguments[0].toString());
    backSocket.send(Array.prototype.slice.call(arguments));
  });

  backSocket.bind(backPort, function (err) {
    console.log('bound', backPort);
  });
}

exports.initZMQ = function(){
  createForwarderDevice(clientPort, serverPort);
  createWorker(serverPort);
};