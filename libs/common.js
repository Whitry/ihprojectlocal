var HashMap = require('hashmap').HashMap;

var request = require('request');

function getQuery(url, callback){
  request(url, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var message = JSON.parse(body);
      //console.log('get request');
      if(message){
        global.updateTime = message.updateTime;
      }
      //console.log(JSON.parse(body));
      callback(body, response.request.path);
    }
  })
}

function postQuery(url, callback){
  request.post(
    url,
    { form: { key: 'value' } },
    function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var message = JSON.parse(body);
        //console.log('get request');
        if(message){
          global.updateTime = message.updateTime;
        }
        //console.log(JSON.parse(body));
        callback(body, response.request.path);
      }
    }
  );
}

function getSensorByTypeAndExternalID(eid, type){
  var res;
  global.sensors.forEach(function(sensor, sensor_id) {
    if( sensor.sensor.system == type && sensor.sensor.external_id == eid ){
      res = sensor;
    }
  });
return res;

}

exports.postQuery = postQuery;
exports.getQuery = getQuery;
exports.getSensorByTypeAndExternalID = getSensorByTypeAndExternalID;

//Common = {
//  sensors: new HashMap()
//};
//global.sensors = Common.sensors;
//module.exports = Common;


/*in file:
 Common = {
 util: require('util'),
 fs:   require('fs'),
 path: require('path')
 };

 module.exports = Common;
* use:
* var Common = require('./common.js');
 console.log(Common.util.inspect(Common));
*
* */