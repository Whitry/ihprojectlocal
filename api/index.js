var restify = require('restify');

//function respond(req, res, next) {
//  res.send('hello ' + req.params.name);
//  next();
//}

var server = restify.createServer({
  formatters: {
    'application/json': function(req, res, body){
      if(req.params.callback){
        var callbackFunctionName = req.params.callback.replace(/[^A-Za-z0-9_\.]/g, '');
        return callbackFunctionName + "(" + JSON.stringify(body) + ");";
      } else {
        return JSON.stringify(body);
      }
    },
    'text/html': function(req, res, body){
      return body;
    }
  }
});
server.use(restify.bodyParser());
//server.get('/hello/:name', respond);
//server.head('/hello/:name', respond);

require('./routes').init(server);

function init(){
  server.listen(8081, function() {
    console.log('%s listening at %s', server.name, server.url);
  });
}


exports.init = init;