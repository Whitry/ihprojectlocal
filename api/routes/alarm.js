var config = require('./../../config');
var alarm = require('./../../app/module/alarm');

function getList(app){
  app.get(config.get("api:url") + config.get("api:version")+ "/alarms", function(req, res, next){
    alarm.listTimers(function(lst, result){
      res.send(result.code, lst);
    });
  });
}

function remove(app){
  app.del(config.get("api:url") + config.get("api:version")+ "/alarm/delete/:name", function(req, res, next) {
    if (req.params.name) {
      alarm.deleteTimer(req.params.name, function(result){
        res.send(result.code, "");
      });
    }
  });
}

function create(app){
  app.post(config.get("api:url") + config.get("api:version")+ "/alarm/create", function(req, res, next) {
    if (req.body) {
      alarm.addTimer(req.body, function(result){
        res.send(result.code, {});
      });
    }
      res.send(201, "");

  });
}

function update(app){
  app.put(config.get("api:url") + config.get("api:version")+ "/alarm/update", function(req, res, next) {
    req.item.set(req.body);
    req.item.save(function(err, item) {
      res.send(204, item);
    });
  });
}

function init(app){

  getList(app);
  remove(app);
  create(app);
  update(app);
}

exports.init = init;