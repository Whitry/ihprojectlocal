
function apiRoutes(app){
  app.post('/api/v10/login', require('./apiRoutes/login').post);
  app.get('/api/v10/sensors', require('./apiRoutes/sensors_list').get);
}

exports.apiRoutes = apiRoutes;