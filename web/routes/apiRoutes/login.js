var User = require('./../../../models/user').User;
var HttpError = require('./../../error').HttpError;
var AuthError = require('./../../../models/user').AuthError;

exports.post = function(req, res, next){
  var username = req.body.username;
  var password = req.body.password;

  User.authorize(username, password, function(err, user){
    if(err){
      if(err instanceof  AuthError){
        return res.send(new HttpError(403, err.message));
      }else{
        return next(err);
      }
    }
    global.apiKeys.set(user._id, new Date());
    //req.session.user = user._id;
    res.send({token: user._id});
  });

//  function(err, user){
//    if(err) return next(err);
//    req.session.user = user._id;
//    res.send({});
//  }

};