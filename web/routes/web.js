

function webRoutes(app){
  //global.zeromqEvent.emit('doorOpen');
  app.post('/login', require('./webRoutes/login').post);
  app.get('/', require('./webRoutes/main_panel').get);
  app.post('/filter_sensors', require('./webRoutes/main_panel').renderSensors);
  //app.get('/filter_sensors', require('./main_panel').renderSensors);
}

exports.webRoutes = webRoutes;