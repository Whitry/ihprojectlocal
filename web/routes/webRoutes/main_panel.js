var mongoose = require('./../../../libs/mongoose'),
  LinkMapSensor = require('./../../../models/map_sensor_link').MapSensorLink,
  iMap = require('./../../../models/map').iMap,
  Sensor  = require('./../../../models/sensor').Sensor,
  HttpError = require('./../.').HttpError,
  ObjectId = require('mongoose').Types.ObjectId;

function filterSensor(req, sensor){
    var isLamp = req && req.session.lamp ? req.session.lamp : 0;
    var isTemp = req && req.session.temp ? req.session.temp : 0;
    var isDoors = req && req.session.doors ? req.session.doors : 0;
    var isAlert = req && req.session.alert ? req.session.alert : 0;
    if( !isLamp && !isTemp && !isDoors ){
        return true;
    }
    if( isLamp && (sensor.type == 2 || sensor.type == 3))
        return true;
    if( isDoors && sensor.type == 4 )
        return true;
    if( isTemp && sensor.type == 1 )
        return true;
    return false;
}

function renderMainPage(req, res, sensor_list){
  res.render('index', {sensors: sensor_list});
}

function renderMobilePage(req, res, sensor_list){
  res.render('/m/index', {sensors: sensor_list});
}

function loadSensorList(req, res, callback) {
  filter(req, res, function(sensor_list){
    var res_list = [];
    for(var sensor_id in sensor_list){
      if( global.sensors.get(sensor_list[sensor_id].sensor.toHexString())){
        res_list.push(sensor_id);
//         var ss = sensor_list[sensor_id];
//         var sensor1 = global.sensors.get(sensor_list[sensor_id].sensor.toHexString());
//         var r = 1;
//         r++;
      }
    }
    callback(req, res, sensor_list);
  });
}

//<%-partial('./partials/sensor_view/' + sensor.getTemplate(), {sensor: sensor})%>
exports.get = function(req, res, next){
  if(!req)res.render('index');
  var ua = req.header('user-agent');
//  if(/mobile/i.test(ua)) {
//    loadSensorList(req, res, renderMobilePage);
//  } else {
    loadSensorList(req, res, renderMainPage);
 // }
};


function filter(req, next, callback){
  loadMap(req, next, function(req, next, one_map){
    loadSensors(req, next, one_map, callback);
  });
}

function loadSensors(req, next, one_map, callback){
  if( !one_map ) callback();
  LinkMapSensor.find({map: new ObjectId(one_map.id)}, function(err, result){
    if(err) return next(err);
    callback( result);
  });
}

function loadMap(req, next, callback){
  var one_map;
  for( one_map in global.iMaps ){
    var currentMap = req && req.session.cmap ? req.session.cmap : 0;
    if( global.iMaps[one_map].map_order == currentMap )
      callback(req, next, global.iMaps[one_map]);
  }
}

function renderSensors(req, res){
  res.render('./partials/sensors', {layout: !req.xhr, data: "123"});
}

exports.renderSensors = function(req, res){
  loadSensorList(req, res, renderSensors);
};