
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./../web/routes');
//var user = require('./web/routes/user');
//var login = require('./web/routes/login');
var http = require('http');
var path = require('path');
var config = require('./../config');
var log = require('./../libs/log')(module);
var mongoose = require('./../libs/mongoose');
var HttpError = require('./error').HttpError;
var wSocket = require('./wsockets');

exports.start = function(){

    var app = express();

// all environments
    app.engine('ejs', require('ejs-locals'));
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');
    app.use(express.favicon(path.join(__dirname, 'public/images/favicon.ico')));

    if( 'development' == app.get('env') ){
        app.use(express.logger('dev'));
    }else{
        app.use(express.logger('default'));
    }
//app.use(express.bodyParser());
    app.use(express.json());
    app.use(express.urlencoded());
//app.use(express.methodOverride());
    app.use(express.cookieParser());

    var MongoStore = require('connect-mongo')(express);

    app.use(express.session({
        secret: config.get('session:secret'),
        key: config.get('session:key'),
        cookie: config.get('session:cookie'),
        store: new MongoStore({mongoose_connection: mongoose.connection})
    }));

//app.use(function(req, res, next){
//  req.session.numberOfVisits = req.session.numberOfVisits + 1 || 1;
//  res.send("Vsisits: " + req.session.numberOfVisits);
//});

app.use(require('./middleware/sendHttpError'));
app.use(require('./middleware/loadUser'));
    app.use(app.router);
    require('./../web/routes')(app);
    app.use(express.static(path.join(__dirname, 'public')));

// development only
//if ('development' == app.get('env')) {
//  app.use(express.errorHandler());
//}



    app.use(function(err, req, res, next) {
    if (typeof err == 'number') { // next(404);
        err = new HttpError(err);
    }

    if (err instanceof HttpError) {
        res.sendHttpError(err);
    } else {
        if (app.get('env') == 'development') {
            express.errorHandler()(err, req, res, next);
        } else {
            log.error(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }
    });

    var webserver = http.createServer(app);
    webserver.listen(config.get('port'), function(){
        log.info('Express server listening on port ' + app.get('port'));
    });
    wSocket.initWebsocket(webserver);
}