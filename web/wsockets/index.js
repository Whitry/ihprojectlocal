var cam = require('./../../libs/camera');
var config = require('./../../config');

/*
* messages:
* msg_info - информационное. Изменение температуры например.
* msg_action - произошло действие. Открылась дверь
* msg_alert - важное сообщение. Протечка и т.п.
*
* */

function init(webserver){
  var io = require('socket.io').listen(webserver, { log: true });
  var res = config.get("socket:port");
  //webserver.listen(config.get("socket:port"));
  global.wsocket = io.sockets;
  io.sockets.on('connection', function (socket) {
   // console.log('client connected: ' + socket.identity);
//    socket.emit('msg_info', { hello: 'world' });
//    socket.on('my other event', function (data) {
//      console.log(data);
//    });
    socket.on('changeValue', function(data){
      console.log('NEW VALUE: ' + data.value);
      var sensor = global.sensors.get(data.id);
      global.sensorIdEvent.emit(sensor.sensor.external_id, sensor, data.value);
      global.sensorTypeEvent.emit(sensor.sensor.getTemplate(), sensor, data.value);
      global.sensorValueChangeByUserEvent.emit('change', sensor, data.value);
    });
    socket.on('changeCameraView', function(data){
      cam.preset.go(data.value);
    });
  });

}

global.sensorTypeEvent.on('door', function(sensor, old_value){
  sendAll(sensor.value);
});

function send(theme, message){
  global.wsocket.emit(theme, { 'somedata':  message  });
}

function sendAll(message){
  send('message', message);
}

function sendInfo(message){
  sendAll('msg_info', message);
}

function sendAction(message){
  sendAll('msg_info', message);
}

function sendAlert(message){
  sendAll('msg_info', message);
}

exports.initWebsocket = init;
exports.sendAll = sendAll;
exports.sendInfo = sendInfo;
exports.sendAction = sendInfo;
exports.sendAlert = sendInfo;