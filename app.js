var appserver = require('./app/server');
appserver.init();

var apiserver = require('./api');
apiserver.init();

var webserver = require('./web/webapp');
webserver.start();