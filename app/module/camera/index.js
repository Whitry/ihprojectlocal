var cam = require('./../../../libs/camera');
var config = require('./../../../config');




// take a picture and store it on your computer
//cam.snapshot("./temp/test.png", function(res){
//});

function refreshImage(){
  cam.setup({
    host: config.get('camera:ip_address'),
    port: config.get('camera:port'),
    user: config.get('camera:user'),
    pass: config.get('camera:password')
  })
  cam.snapshot("./web/public/camera/screen.png", function(res){
  });
}

function getImage(){
  setInterval(refreshImage, 500);
}


exports.init = getImage;