/*
 * Оповещение по разным каналам
 *
 * */
var SMSru = require('sms_ru');
var config = require('./../../../config');

global.sensorTypeEvent.on('door', function(sensor, old_value){
  if( sensor.value == 'true' ){
    var sms = new SMSru('2b95541f-0056-1cd4-3926-95d3d792d2a0');
    sms.sms_send({
      to: config.get('notification:sms'),
      text: config.get('demo:notif_text')
    }, function(e){
      console.log(e.description);
    });
  }
});

function init(){

}

exports.init = init;