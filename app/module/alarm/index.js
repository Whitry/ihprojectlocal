var mongoose = require('./../../../libs/mongoose');
var configDb = require('./../../../models/config').iConfig;
var cfg = require('./../../../config');
var crud = require('./timerCRUD');
var HashMap = require('hashmap').HashMap;
var schedule = require('node-schedule');
/*
* config format
* {
*   name: String,
*   repeat: Boolean //repeat every day
*   runtime{
*     hh: Integer
*     mm: Integer
*     dd: Integer //optional
*     mm: Integer // optional
*
*   }
* }
*
* */

function parseSettings(data){
  for(var timers in data){
    console.log(data[timers]);
  }
}

function clearTimers(){
  global.alarms.forEach(function(value, key) {
    schedule.cancelJob(value);
  });
  global.alarms.clear();
}

function loadSettings(){
  clearTimers();
  configDb.findOne({key: cfg.get("alarm:config_key")}, function(err, res){
    parseSettings("" === res.value ? "" : JSON.parse(res.value));
  });

}

function init(){
  global.alarms = new HashMap();
  loadSettings();
}

exports.init = init;
exports.addTimer = function(newTimer, callback){
  crud.addTimer(newTimer, callback);
};
exports.updateTimer = function(newTimer, callback){
  crud.updateTimer(newTimer, callback);
};
exports.deleteTimer = function(newTimer, callback){
  crud.deleteTimer(newTimer, callback);
};

exports.listTimers = function(callback){
  crud.listTimers(callback);
};
