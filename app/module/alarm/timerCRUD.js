var configDb = require('./../../../models/config').iConfig;
var cfg = require('./../../../config');


function updateTimer(value, updateTime, result){
  return addTimer(deleteTimer(value, updateTime.name, result), updateTime, result);
}

function deleteTimer(value, deleteTimer, result){
  if( "" != value ){
    var timers = JSON.parse(value);
    for( var timer in timers ){
      if(null != timers[timer] && null != timers[timer].name
        && deleteTimer === timers[timer].name){
        delete timers[timer];
        break;
      }
    }
    //in array some field is null. Let's clear it
    var cleanTimers = [];
    for( var timer in timers ){
      if( null != timers[timer] ){
        cleanTimers.push(timers[timer]);
      }
    }
    result.code = 204;
    return JSON.stringify(cleanTimers);
  }
  result.code = 500;
  result.errorText = "Неверный формат запроса";
  return value;
}

function addTimer(value, newTimer, result){
  if( value === "" ){
    result.code = 201;
    return "[" + newTimer + "]";
  }
  var timers = JSON.parse(value );
  var newTimerJson = JSON.parse(newTimer );
  for( var timer in timers ){
    if(null != timers[timer] && null != timers[timer].name
      && newTimerJson.name === timers[timer].name){
      result.code = 400;
      return JSON.stringify(timers);
    }
  }
  timers.push(newTimerJson);
  result.code = 201;
  return JSON.stringify(timers);
}

function listTimers(callback, result){
  configDb.findOne({key: cfg.get("alarm:config_key")}, function(err, res){
    if( err ){
      res = new config();
      res.key = cfg.get("alarm:config_key");
      callback("");
      result.code = 404;
      result.errorText = "Конфигурация будильника не найдена";
    }else{
      result.code = 200;
    }
    callback(res.value,result);
  });
}

function saveSettings(data, callback, resultCallback){
  configDb.findOne({key: cfg.get("alarm:config_key")}, function(err, res){
    if( err ){
      res = new config();
      res.key = cfg.get("alarm:config_key");
    }
    // result - результат выполнения. Содержит code= 200, 202; error = "Некоторая ошибка"
    var result = [];
    res.value = callback(res.value, data, result);
    res.save(function(res, err){if(err) console.log(err);});
    if( undefined != resultCallback ){
      resultCallback(result);
    }
  });
}

exports.addTimer = function(data, callback){
  saveSettings(data, addTimer, callback);
};
exports.updateTimer = function(data, callback){
  saveSettings(data, updateTimer, callback);
};
exports.deleteTimer = function(data, callback){
  saveSettings(data, deleteTimer, callback);
};

exports.listTimers = function(callback){
  var result = [];
  listTimers(callback, result);
}

