//http://192.168.1.7:8083/ZWaveAPI/Run/devices[6].instances[0].commandClasses[0x26].Set(69)
var common = require('./../../../libs/common');
var config = require('./../../../config');

function end(){
  setTimeout(demo, config.get("demo:demo_delay"));
}

function someTurnOn(){
  //ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(0)
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[7].instances[2].commandClasses[0x26].Set(255)';
  setTimeout(function(){
    common.postQuery(url, end);
  }, config.get("demo:device_delay"));
}
function someTurnOff(){
  //ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(0)
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[7].instances[2].commandClasses[0x25].Set(0)';
  setTimeout(function(){
    common.postQuery(url, someTurnOn);
  }, config.get("demo:device_delay"));
}
function zalTurnOn(){
  //ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(0)
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(255)';
  setTimeout(function(){
    common.postQuery(url, someTurnOff);
  }, config.get("demo:device_delay"));
}

function zalTurnOff(){
  //ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(0)
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[8].instances[0].commandClasses[0x26].Set(0)';
  setTimeout(function(){
    common.postQuery(url, zalTurnOn);
  }, config.get("demo:device_delay"));
}

function dimmerTurnOn(){
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[6].instances[0].commandClasses[0x26].Set(100)';
  setTimeout(function(){
    common.postQuery(url, zalTurnOff);
  }, config.get("demo:device_delay"));
}

function dimmerTurnOff(){
  var url = config.get('zwave:server')+'ZWaveAPI/Run/devices[6].instances[0].commandClasses[0x26].Set(0)';
  setTimeout(function(){
    common.postQuery(url, dimmerTurnOn);
  }, config.get("demo:device_delay"));

}

function demo(){
  dimmerTurnOff();
}

exports.init = demo;