var valueChanger = require('./value_changer');


function ZWaveSensorWorker(sensor){
  this.sensor = sensor;
  this.msg_queue = new Array();
  this.online = false;
  this.value_in_changing = false; //
  global.sensorIdEvent.on(sensor.external_id, function( sensor, value){
    value_changed(sensor, value, sensor.msg_queue);
  });
}

function transformValue(sensor, value){
  switch (sensor.sensor.type) {
    case 2:
      return value;
    case 3:
      return value;
    case 4:
      return value;
    case 5:

      return Math.round(255/100*parseInt(value));
    case 6:
      return value;
  }
  return value;
}

function value_changed(sensor, value, msg_queue){
  value = transformValue(sensor, value);
  if( sensor.sensor.value == value ){
    return;
  }
  if( msg_queue.length > 2 ){
    console.log('remove');
    msg_queue = new Array();
    msg_queue.push(value);
  }
  msg_queue.push(value);
  if( sensor.value_in_changing ){
    return;
  }
  sensor.value_in_changing = true;
  changeValueOnZWave(sensor, msg_queue.shift());
}

function changeValueOnZWave(sensor, value){
  valueChanger.changeValue(sensor, value);
}

ZWaveSensorWorker.prototype.valueChaned = function(){
  console.log('VALUE CHANGED');
  this.value_in_changing = false;
}


ZWaveSensorWorker.prototype = require('./../../core/sensor_worker').SensorWorker;

exports.ZWaveSensorWorker = ZWaveSensorWorker;