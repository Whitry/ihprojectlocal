var Common = require('./../../../libs/common.js');
var Parser = require('./parser.js');
var EventGenerator = require('./event_generator');
var zwaveServer = require('./zwave_server');
var changer = require('./value_changer');

function parse(message ){
  //Common.sensors;
  EventGenerator.generateEvent(Parser.parse(message));
}




function init(){
  setInterval(zwaveServer.worker, 2000);
  zwaveServer.init();
  changer.init();
}



//global.zeromqEvent.on('message', function(message){
//  console.log('message event emitter! ' + message);
//  parse(message);
//});


exports.init = init;

