var MessageDO = require('./message_do.js');
/*
* Для изменения показания датчиков возникают следующие события:
* Событие по конретному датчик
* id#New_value Напримре 34#17 - датчик с id 34 изменил текущее значение на 17
* Событие по такому типу датчика
* type#id#value
* */
function  generateEvent(message){
  if(message.type == 1){
    global.sensorTypeEvent.emit('door_window', message);
  }
}

exports.generateEvent = generateEvent;