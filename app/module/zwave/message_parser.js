var common = require('./../../../libs/common');

//devices.1.instances.0.commandClasses.32.data.level

function parse(data){
  var jData = JSON.parse(data);
//  if( !new String(data).contains('devices'))
//    return;
  for( var device_id in jData ){

    var device = jData[device_id];
    var ar = device_id.split('.')
    if( ar.length < 7 )
      continue;
    var external_id = ar[0] + '.' + ar[1] + '.' + ar[2] + '.' + ar[3]+'.data';
    var sensor = common.getSensorByTypeAndExternalID( external_id, 'z-wave' );
    if( sensor ){
      parseSensorData(sensor, ar[5], device);
    }
  }
}
function parseSensorData(sensor, cmd, data){
  var value;
  if( '48' == cmd ){
    value = getValue48(data);
    sensor.last_update = getLastUpdate48(data)*1000;

  }

  if( '49' == cmd ){
    value = getValue49(data);
    sensor.last_update = getLastUpdate49(data)*1000;

  }
  ///devices[12].instances[1].commandClasses[0x26].Set(32)
  //
  if(value != null && value != undefined && value.toString() != sensor.value){
    var old_value = sensor.value;
    sensor.value = value;
    sensor.sensor.save();
    var message = new Object();
    message.old_value = old_value;
    message.sensor = sensor;
    global.sensorIdEvent.emit(sensor.external_id, sensor, old_value );
    global.sensorTypeEvent.emit(sensor.sensor.getTemplate(),  sensor, old_value );
    console.log('value: ' + sensor.value);
    console.log('last_update: ' + sensor.last_update);
  }

}

function getLastUpdate48(data){
  return data.level.updateTime;
}

function getValue48(data){
  return data.level.value;
}


function getLastUpdate49(data){
  return data.val.updateTime;
}

function getValue49(data){
  return data.val.value;
}

//function getValue(instance){
//
//}
//
//function getValue(instance){
//
//}

exports.parse = parse;