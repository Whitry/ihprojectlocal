var common = require('./../../../libs/common');
var config = require('./../../../config');

function serverWorker(data){
  //console.log(data);
  require('./message_parser').parse(data);
}

function worker(){
  var dt1 = new Date();
  var url = config.get('zwave:server')+'ZWaveAPI/Data/'+
    (global.updateTime ? global.updateTime : Math.round(dt1.getTime()/1000));
  common.postQuery(url, serverWorker);
}

function initZWave(){
  var url = config.get('zwave:server')+'ZWaveAPI/Data/0';
  console.log(url);
  common.getQuery(url, require('./init_parser').parse);
}


exports.init = initZWave;
exports.worker = worker;