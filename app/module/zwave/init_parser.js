var common = require('./../../../libs/common');

function parse(data){
  var jData = JSON.parse(data);

  for( var device_id in jData.devices ){
    if(1 == device_id )
      continue;

    var device = jData.devices[device_id];

    for( var instance_id in device.instances ){

      var instance = device.instances[instance_id];
      console.log('id: ' + getId(instance));
      console.log('tech_name: ' + getTechnicalname(instance));
      var sensor = common.getSensorByTypeAndExternalID( getTechnicalname(instance), 'z-wave' );
      if(sensor){
        sensor.online = true;
      }
      var icommandClass = getCommandClassData(instance);
      if( icommandClass ){
        if(sensor)
          parseSensorData(sensor.sensor, icommandClass, instance.commandClasses[icommandClass]);
//        console.log('value: ' + getValue(icommandClass));
//        console.log('last_update: ' + getLastUpdate(icommandClass));
      }
    }
  }
}

function getId(instance){
  return instance.data.genericType.value;
}

function getTechnicalname(instance){
  return instance.data.name;
}

// функция, которая хранит текущее значение датчика
function getCommandClassData(instance){
  for(var func_id in instance.commandClasses){
    if(48 == func_id || 49 == func_id || 38 == func_id){
      return func_id;
    }
  }
  return null;
}

function parseSensorData(sensor, cmd, data){
  if( 48 == cmd ){
    sensor.value = getValue48(data);
    sensor.last_update = getLastUpdate48(data)*1000;

  }

  if( 49 == cmd ){
    sensor.value = getValue49(data);
    sensor.last_update = getLastUpdate49(data)*1000;
    console.log('value: ' +  getValue49(data));
  }

  if( 38 == cmd ){
    sensor.value = getValue38(data);
    sensor.last_update = getLastUpdate38(data)*1000;
    console.log('value: ' +  getValue38(data));
  }
  sensor.save();
  console.log('value: ' + sensor.value);
  console.log('last_update: ' + sensor.last_update);
}

function getLastUpdate48(data){
  return data.data[1].level.updateTime;
}

function getValue48(data){
  return data.data[1].level.value;
}


function getLastUpdate49(data){
  return data.data[1].val.updateTime;
}

function getValue49(data){
  return data.data[1].val.value;
}

function getValue38(data){
  return data.data.level.value;
}

function getLastUpdate38(data){
  return data.data.level.updateTime;
}

//function getValue(instance){
//
//}
//
//function getValue(instance){
//
//}

exports.parse = parse;