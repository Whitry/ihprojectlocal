var common = require('./../../../libs/common');
var config = require('./../../../config');

function generateExternalId(device, inst){
  return "devices."+device.replace(/\[|\]/g, '')+".instances." + inst.replace(/\[|\]/g, '') + '.data';
}

function parseResult(data, path){
  console.log("VALUE CHANGED: " + path);

  // получаем external_id из запроса

  var numb = path.match(/\[\d+\]/g);
  console.log(numb);
  if( 2 === numb.length ){
    sensor = common.getSensorByTypeAndExternalID(generateExternalId(numb[0], numb[1]), 'z-wave');
    sensor.value_in_changing = false;
    // получаем значение
    var val = path.match(/\(\d+\)/g);
    if( 1 == val.length ){
      sensor.sensor.value = val[0].replace(/\(|\)/g, '')
      sensor.sensor.save();
    }
  }
}

function transformNameForRequest(id){
  var datas = id.split('.');
  return 'devices[' + datas[1] + '].instances['+datas[3]+'].';
}

function generateUrl(sensor, newValue){
  switch (sensor.type) {
    case 2:
      return "switcher";
    case 3:
      return config.get('zwave:server') + "ZWaveAPI/Run/" + transformNameForRequest(sensor.external_id) + "commandClasses[0x26].Set("+newValue+")";
    case 4:
      return "door";
    case 5:

      return config.get('zwave:server') + "ZWaveAPI/Run/" + transformNameForRequest(sensor.external_id) + "commandClasses[0x26].Set("+newValue+")";
    case 6:
      return "";
  }
}

function changeValue(sensor, newValue){
  var url = generateUrl(sensor.sensor, newValue);
  common.postQuery(url, parseResult);
}

function init(){
//  global.sensorValueChangeByUserEvent.on('change', function(sensor, value){
//    console.log('VALUE CHANGED: ' + sensor.sensor.name + ' ' + value);
//  });
}

exports.changeValue = changeValue;
exports.init = init;