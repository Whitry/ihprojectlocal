/*
* Панель сообщений
* подписывается на сообщения от датчиков
* и добавляет событие если нужно
* Типы событий:
* информационное, внимание, важное
* 
* */

global.sensorTypeEvent.on('door', function(sensor, old_value){
  console.log('door_window event emitter! ' + old_value);
  global.eventCreateEvent.emit("info");
});

function init(){

}

exports.init = init;