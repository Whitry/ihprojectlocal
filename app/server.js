// app server main file
var HashMap = require('hashmap').HashMap;
var events = require('events');
var core = require('./core');
var config = require('./../config');


function init_module(){
//  var zmq = require('./../libs/zeromq_worker');
//  zmq.initZMQ()
  var zwave = require('./module/zwave');
  zwave.init();
  var eventpanel = require('./module/eventpanel');
  eventpanel.init();
  var externla_eventer = require('./module/external_eventer');
  externla_eventer.init();
  var camera = require('./module/camera').init();
//  var demo = require('./module/demonstrate');
//  demo.init();
}

function init_events(){
  // очередь сообщений от zeromq
  global.zeromqEvent = new events.EventEmitter();
  // очередь сообщений на конкретный датчик
  global.sensorIdEvent = new events.EventEmitter();
  // очередь сообщений рассылаемых при изменении значения конкретного типа датчиков
  // global.zwaveEvent.on('door_window', function(){
  // console.log('event emitter!');
  // });
  global.sensorTypeEvent = new events.EventEmitter();
  // событие при изменении значения датчика
  global.sensorValueChangeByUserEvent = new events.EventEmitter();
  // событие при изменении значения датчика
  global.sensorValueChangeBySystemEvent = new events.EventEmitter();
  // создание кастомного события
  // Типы событий
  // info - информационное
  // action - важное действие
  // alert - критичное событие, возможно оповещение
  global.eventCreateEvent = new events.EventEmitter();
}

function initObjects(){
  global.sensors = new HashMap();
  global.sensorsByExternal = new HashMap();
  core.init();
}



function init(){
  console.log('Init objects');
  initObjects();
  console.log('Init events');
  init_events();
  console.log('Init module');
  init_module();

}

exports.init = init;