var mongoose = require('./../../libs/mongoose');
var Sensor = require('./../../models/sensor').Sensor;
var ZWaveSensorWorker = require('./../module/zwave/zwave_sensor_worker').ZWaveSensorWorker;
var SensorWorker = require('./../core/sensor_worker').SensorWorker;

function createWorker(sensor){
  if( 'z-wave' == sensor.system ){
    return new ZWaveSensorWorker(sensor);
  }
  return new SensorWorker(sensor);
}

function load(){
  //res.send("respond with a resource");
  Sensor.find(function(err, result){
    if(err) throw err;
    for(var sensor in result){
      var worker = createWorker(result[sensor]);
      global.sensors.set(result[sensor].id, worker);
      global.sensorsByExternal.set(result[sensor].external_id, worker);
      //var ss = global.sensors.get(result[sensor].id);
    }


  });

}

exports.load = load;