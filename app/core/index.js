var maps = require('./load_maps');
var sensors = require('./load_sensor');

function init(){
  maps.load();
  sensors.load();
}

function findSensorByExternalId(externalId){
  var res;
  global.sensors.forEach(function(sensor, sensor_id) {
    if( sensor.sensor.external_id == externalId ){
      res = sensor;
    }
  });
  return res;
}

exports.init = init;
exports.findSensorByExternalId = findSensorByExternalId;
